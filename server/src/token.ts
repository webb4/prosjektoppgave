// let jwt = require('jsonwebtoken');
import { User } from '../../types';
import * as jwt from 'jsonwebtoken'
let secret: string;  // if the secret has type string | undefined jwt gets errors with overload calls

if (process.env.SECRET){
    secret = process.env.SECRET;
} else {
    throw new Error("Lacking environmental variable SECRET");
}

export function generateToken(user: User){
    return jwt.sign({username : user.username, id: user.id, type: user.type}, secret, {expiresIn: '24h'})
}

export function verifyToken(token: string){
    return new Promise<jwt.JwtPayload>((resolve, reject) => {
        jwt.verify(token, secret, (error, answer) => {
            if (error) reject(error);
            
            if (answer == undefined) {
                reject("No answer given")
            } else  {
                resolve(answer)
            }
        });      
    })
}
