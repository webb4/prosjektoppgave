import mysql from 'mysql';

export const pool = mysql.createPool({
      host: process.env.MYSQL_HOST,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: process.env.MYSQL_DATABASE,
      // Reduce load on NTNU MySQL server
      connectionLimit: 10,
});

export const tempPool = mysql.createPool({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_TEMP_DATABASE,
  // Reduce load on NTNU MySQL server
  connectionLimit: 1,
});

export const testPool = mysql.createPool({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_TEST_DATABASE,
  // Reduce load on NTNU MySQL server
  connectionLimit: 1,
});


