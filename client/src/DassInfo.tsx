import * as React from 'react';
import StarRatings from 'react-star-ratings';
import { Alert, Card, CardGroup, Carousel, Col, Container, Form, Image, ListGroup, ListGroupItem, Pagination, Row, Button } from 'react-bootstrap';
import { getCookie } from './utils'
import { Link } from "react-router-dom";

let url: string = "http://localhost:3001/api/v1";
import { Toilet, Review, Vote } from '../../types'
import { makeMazeMapInstance, MazeMapWrapper } from './mazemagic';
import { Mazemap } from "mazemap";
import { getReviewVotes, getToilet, getToiletReviews, upvoteReview } from './fetch-service';



type MyProps = { };
type ReviewProps =  {
    review: Review,
}
type ReviewsProps= { reviews: Review[] }
type ReviewsPropsPag= { reviews: Review[], onPageChange: any, }
type PaginationState = { reviews: Review[], start: number }
type MyState = { 
    toilet:Toilet, 
    reviews: Review[],
    isError: boolean, 
    errorMsg: string,
    loading: boolean,
    review: Review,
    toiletZLevel: number,
};
const campusId = 1;
const map = makeMazeMapInstance({campuses: campusId }, "fill");
export class DassInfo extends React.Component<MyProps, MyState> {
    constructor(props: MyProps) {
        super(props);
        this.state = {
            toilet: {
                id: undefined,
                name: undefined,
                description: undefined,
                gender: undefined,
                facilities: undefined,
                poi_id: undefined,
                campus_id: undefined,
                avg_rating: undefined,
                image_link: undefined,
                creator_user_id: undefined,
                calculated_avg: undefined,
            }, 
            reviews: [],
            isError: false,
            errorMsg: "",
            loading: true,
            review: {
                id: undefined,
                user_id: undefined,
                toilet_id: undefined,
                title: undefined,
                username: undefined,
                toilet_rating: undefined,
                review_rating: undefined,
                description: undefined
            },
            toiletZLevel: 0,

        }
    }
    
    componentDidMount() {
        //https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams/get
        // Får tak i id gjennom URL-parameter.
        let params = new URLSearchParams(document.location.search.substring(1));
        let id = params.get("id");
        if (id!==null) {
            getToilet(id).then((response: any) => this.setState({ 
                toilet: response, 
                loading: false,
            })
            )
            .catch((error: { message: any; }) => this.setState({ isError: true, errorMsg: error.message, loading: false }));

            getToiletReviews(id).then(response => this.setState({ 
                reviews: response, 
                loading: false,
            })
            )
            .then(() => { //Gets votes for each review, pushes to tempArr?
                this.state.reviews.map((rev) => {
                    fetch(url + "/reviews/" + rev.id + "/votes", {
                        method: 'GET',
                        credentials: 'include',
                        mode: 'cors',
                        headers: {
                            'Content-Type' : 'application/json'
                        }
                    }).then(response => response.json())
                    .then(data => {
                        rev.review_rating = Number(data.shift()["SUM(vote)"]);
                        console.log("HHHEHEEEEERRR")
                        console.log(rev.review_rating);
                    })
                })
            })
            .catch(error => this.setState({ isError: false, errorMsg: error.message, loading: false }));
        }
        else this.setState({ isError: true, errorMsg: "Invalid id."});

        map.on('load', () => {
            map.highlighter = new Mazemap.Highlighter(map, {
                showOutline: true,
                showFill: true,
                outlineColor: Mazemap.Util.Colors.MazeColors.MazeBlue,
                fillColor: Mazemap.Util.Colors.MazeColors.MazeBlue
            } );

            Mazemap.Data.getPoi(this.state.toilet.poi_id).then((poi: any) => {
                placeToiletMarker(poi, this.state.toilet.id);
                this.setState({ toiletZLevel: poi.properties.zLevel });
            });
            function placeToiletMarker(poi: any, toiletid: number | undefined) {
                var lngLat = Mazemap.Util.getPoiLngLat(poi);
                map.setCenter(lngLat);
                var popup = new Mazemap.Popup({closeOnClick: true, offset: [0, -30]})
                .setHTML('<a href = "toilets/?id='+toiletid + '">' + poi.properties.title + '</a>');
            
            
                var mazeMarker = new Mazemap.MazeMarker({
                    color: '#ff00cc',
                    innerCircle: true,
                    innerCircleColor: '#FFF',
                    size: 34,
                    innerCircleScale: 0.5,
                    zLevel: poi.properties.zLevel,
                })
                .setPopup(popup)
                .setLngLat(lngLat)
                .addTo(map);    

                // If we have a polygon, use the default 'highlight' function to draw a marked outline around the POI.
                if(poi.geometry.type === "Polygon"){
                    map.highlighter.highlight(poi);
                }
            }
        });
    }
    genderToHumanReadable = (gender: number | undefined) => {
        switch(gender) {
            case 0: return "Both";
            case 1: return "Man"
            case 2: return "Woman"
            case 3: return "Handicap"
            case 4: return "Other" 
        }
    }
    render() {
        return (
            <Container fluid>
                <Row >
                {this.state.isError ? <Alert variant="danger">{this.state.errorMsg}</Alert>:""}
                <Col>
                <Card border="primary" bg="dark" text="white" style={{ width: '100%'}}>
                    <Card.Img variant="top" src={this.state.toilet.image_link} style= {{ maxHeight: '600px' }} />
                    <Card.Body>
                        <Card.Title>{this.state.toilet.name}</Card.Title>
                        <Card.Text>Gender: {this.genderToHumanReadable(this.state.toilet.gender)}</Card.Text>
                        <Card.Text>Facilities: {this.state.toilet.facilities}</Card.Text>
                        <Card.Text>Description: {this.state.toilet.description || <em>No description</em>}</Card.Text>
                        <Card.Text>Etasje: {this.state.toiletZLevel}</Card.Text>
                        
                        <StarRatings
                        rating={this.state.toilet.avg_rating || 0}
                        starDimension="30px"
                        starSpacing="3px"
                        numberOfStars={6}
                        starRatedColor="gold"
                    />  
                        <br/>
                        <Link to={"/reviews?id=" + this.state.toilet.id}><Card.Link>Give your own review</Card.Link></Link>
                    </Card.Body>
                </Card>
                </Col>
                <Col md={8}>
                { this.state.loading ? <div>Loading map</div> : <MazeMapWrapper map={map} /> }
                </Col>
                {this.state.errorMsg === "404" ? <Card><Card.Title>No reviews</Card.Title></Card> : (
                    this.state.reviews.length > 0 && <ReviewPagination reviews={this.state.reviews}/>
                )}
                </Row>
            </Container>
        );    
    }
}

class ReviewPagination extends React.Component<ReviewsProps, PaginationState> {
    constructor(props: ReviewsProps) {
        super(props);
        this.state = {
            reviews: [],
            start: 0,
        }
    }
    handlePageChange = (start: number) => {
        this.setState( { start })
        this.render();
    }
    render() {
        let stop = this.state.start+4;
        let slicedReviews = this.props.reviews.slice(this.state.start, stop);   
        return (
            <div>
                <CardGroup>
                    {slicedReviews.map(rev => <ReviewCard review={rev}/>)}
                </CardGroup>
                <Pagination>
                    <ReviewPaginationButtons reviews={this.props.reviews} onPageChange={this.handlePageChange}/>
                </Pagination>
            </div>
        )
    }
}

class ReviewPaginationButtons extends React.Component<ReviewsPropsPag> {
    constructor(props: ReviewsPropsPag) {
        super(props)
    }   
    handleClickChange = (event: { currentTarget: { id: string; }; }) => {
        this.props.onPageChange(parseInt(event.currentTarget.id));
    }
    
    render() {
        let start = 0;
        let stop = Math.round(this.props.reviews.length/4);
        let paginationButtons = [];
        for (let i=start;i<stop;i++) {
            paginationButtons.push(
                <Pagination.Item 
                    key={"paginationbutton"+i}
                    id={(i*4).toString()} 
                    onClick={this.handleClickChange.bind(this)}
                >
                    {i+1}
                </Pagination.Item>
            )
        }
        if (stop>0) {
            return paginationButtons;
        }
    else {
        console.log("No reviews")
        return (
            <Pagination.Item key={1}>
                1
            </Pagination.Item>
        )
    }
    }
}

class ReviewCard extends React.Component<ReviewProps, MyState> {
    constructor(props: ReviewProps) {
        super(props);
        this.state = {
            toilet: {
                id: undefined,
                name: undefined,
                description: undefined,
                gender: undefined,
                facilities: undefined,
                poi_id: undefined,
                campus_id: undefined,
                avg_rating: undefined,
                image_link: undefined,
                creator_user_id: undefined,
                calculated_avg: undefined,
            }, 
            reviews: [],
            isError: false,
            errorMsg: "",
            loading: true,
            review: {
                id: undefined,
                user_id: undefined,
                toilet_id: undefined,
                title: undefined,
                username: undefined,
                toilet_rating: undefined,
                review_rating: undefined,
                description: undefined
            },
            toiletZLevel: 0
        }
    }
    updateVote(){
        getReviewVotes(this.state.review.id)
        .then(data => {
            //@ts-ignore
            this.props.review.review_rating = Number(data.shift()["SUM(vote)"]);
        })
        .then(() => {
            this.forceUpdate();
        })
    }

    render() {
        let hasRating = this.props.review.toilet_rating !== null;
        return (
            <Card border="primary" key={"review"+this.props.review.id}>
                <Card.Header>{this.props.review.username || <em>User deleted</em>}</Card.Header>
                <Card.Body>
                    <Card.Title>{this.props.review.title || <em>No title</em>}</Card.Title>
                    <Card.Text>{this.props.review.description || <em>No description</em>}</Card.Text>
                    {hasRating ? ( //Shows rating with stars, if there is one.
                        <StarRatings
                            rating={this.props.review.toilet_rating}
                            starDimension="30px"
                            starSpacing="3px"
                            numberOfStars={6}
                            starRatedColor="gold"
                        />
                    ): "No rating"
                    }
                    <Card.Footer>
                        <Col>
                            <Button variant="outline-success" onClick={e => {
                                let token = getCookie("jwt");
                                if(!token){
                                    alert("You need to sign in.");
                                }
                                else{
                                upvoteReview(this.props.review.id)
                                .then(() => {
                                    getReviewVotes(this.props.review.id)
                                })
                                .then(e => {
                                    //@ts-ignore
                                    this.props.review.review_rating = Number(e[0]["SUM(vote)"])
                                })
                                .catch(err => console.error(err));}
                                // this.componentDidMount();
                                this.updateVote();
                            }}>Nice review</Button>
                        </Col>
                        <Col>
                            <Button variant="outline-danger" onClick={e => {
                                let token = getCookie("jwt");
                                if(!token){
                                    alert("You need to sign in.");
                                }
                                else{
                                fetch(url + "/reviewVotes/" + this.props.review.id + "/down", {
                                    method: 'POST', // *GET, POST, PUT, DELETE, etc. 
                                    credentials: 'include',
                                    mode: 'cors', // no-cors, *cors, same-origin *1/ 
                                    headers: { 
                                        'Content-Type': 'application/json',
                                        'Authorization': `${token}`
                                        // 'Content-Type': 'application/x-www-form-urlencoded', 
                                    }, 
                                }).then(e=> {
                                    console.log(e);
                                }).then(() => {
                                    fetch(url + "/reviews/" + this.props.review.id + "/votes", {
                                        method: 'GET',
                                        credentials: 'include',
                                        mode: 'cors',
                                        headers: {
                                            'Content-Type' : 'application/json'
                                        }
                                    }).then((e) => {
                                        (e.json().then(e => {
                                            this.props.review.review_rating = Number(e[0]["SUM(vote)"])
                                        }));
                                        //console.log(Number(e[0]["SUM(vote)"]));
                                        //this.props.review.review_rating = Number(e[0]["SUM(vote)"]);
                                    })
                                })
                                .catch(err => console.error(err));}
                                // this.componentDidMount();
                                this.updateVote();
                            }}>Useless review</Button>
                        </Col>
                        <Col className="justify-content-end">
                            Review rating: {this.props.review.review_rating}
                        </Col>
                    </Card.Footer>
                </Card.Body>
            </Card>
        )
    }
}
