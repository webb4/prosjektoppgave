import * as React from 'react';
import { Alert } from './wigdets'
import { Navigate } from "react-router-dom"

let url: string = "http://localhost:3001/api/v1"
type Test ={
    username : string;
    password : string;
    password2 : string;
    alert: string | undefined;
    navigate: boolean;
}
type SendData = {
    username : string;
    password : string;
}

export default class Signup extends React.Component {
    type: "danger" | "success"
    state :Test = {
        username: "",
        password: "",
        password2: "",
        alert: undefined,
        navigate: false
    }
    constructor(props: any){
        super(props)
        this.type = "danger" //used to spesify alert type, danger = red, success = green
    }
    render(){
        return(
            <div className="container" >
                {this.state.navigate ? <Navigate to="/login"/>: ""}
                {this.state.alert ? <Alert type={this.type} message={this.state.alert}/>:""}
                <h1> Signup </h1>
                    <div className="form-group">
                        <label>
                            <b>Username:</b>
                            <input placeholder="Username" className="form-control" type="text" onChange={e => (this.setState({ username: e.target.value}))} />
                        </label>
                    </div>
                    <div className="form-group">
                        <label>
                            <b>Password:</b>
                            <input  className="form-control" placeholder="password" type="password" onChange={e => (this.setState({ password: e.target.value}))}/>
                        </label>
                    </div>
                    <div className="form-group">
                        <label>
                            <b>Retype password:</b>
                            <input  className="form-control" placeholder="password" type="password" onChange={e => (this.setState({ password2: e.target.value}))}/>
                        </label>
                    </div>
                        <button className="btn btn-primary" type="submit" onClick={this.signup.bind(this)}>Signup</button>
            </div>
        )
    }
    signup(){
        setTimeout(() => {
            this.setState({alert: undefined})
        }, 2000);
        let data: SendData = {
            username: this.state.username,
            password: this.state.password
        };
        if(this.state.username == "" || this.state.password == ""){
            this.setState({alert : "You must spesify a username and password"})
            return
        }
        if (this.state.password !== this.state.password2){
            this.setState({alert : "Password must match up"})
        }

        fetch(url + "/signup", {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            /* mode: 'no-cors', // no-cors, *cors, same-origin */
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        })
            .then(response => {
                if(response.status != 201){
                    response.json()
                    .then((data) =>{
                        if(data == "duplicate user"){
                            this.setState({alert : "Username taken"})
                        }
                        else{
                            console.log("noe", data)
                            this.setState({alert : "Something went wrong"})
                        }
                    })
                        .catch(() => this.setState({alert : "Something went wrong"}))

                }
                else {
                    response.text()
                    .then((data) =>{
                        console.log("created user")
                        this.type = "success"
                        this.setState({alert : "Success"})
                        this.type = "danger"
                        this.setState({navigate: true})
                    })
                        .catch((e => this.setState({alert :"something went wrong"})))
                }

            })
            .catch(() => this.setState({alert : "Could not connect to the database"}))
    }
}
