import * as React from 'react';
import { MazeMapWrapper, makeMazeMapInstance } from './mazemagic';
import { Mazemap } from "mazemap"; 

import "../node_modules/mazemap/mazemap.min.css"
import { Toilet } from '../../types';

type MyProps = { };
type MyState = { 
    toilets:Toilet[], 
    error: boolean, 
    loading: boolean 
};

let url: string = "http://localhost:3001/api/v1";

const campusId = 1;
const map = makeMazeMapInstance({campuses: campusId }, "mapRoot");

export class RenderMap extends React.Component<MyProps, MyState> {
    constructor(props: MyProps) {
        super(props);
        this.state = {
            
            toilets: [],
            error: false,
            loading: true,
        }
    }


    componentDidMount() {
        console.log("componentDidMount"); //Check if component is mounted

        fetch(url + "/toilets", {
            method: 'GET', // *GET, POST, PUT, DELETE, etc. 
            credentials: 'include',
            mode: 'cors', // no-cors, *cors, same-origin *1/ 
            headers: { 
                'Content-Type': 'application/json' 
                // 'Content-Type': 'application/x-www-form-urlencoded', 
            }, 
        }) 
        .then(response => response.json()) 
        .then(response => this.setState({ 
            toilets: response, 
            loading: false 
        })) 
        .catch(error => this.setState({ 
            error: true, 
            loading: false 
        })); 

        
        map.on('load', () => {
            map.highlighter = new Mazemap.Highlighter(map, {
                showOutline: true,
                showFill: true,
                outlineColor: Mazemap.Util.Colors.MazeColors.MazeBlue,
                fillColor: Mazemap.Util.Colors.MazeColors.MazeBlue
            } );

            this.state.toilets.map(toilet => {
                Mazemap.Data.getPoi(toilet.poi_id).then((poi: Object) => {
                    console.log(poi);
                    placeToiletMarker(poi, toilet.id);
                });
            });
            function placeToiletMarker(poi: any, toiletid: number | undefined) {
                var lngLat = Mazemap.Util.getPoiLngLat(poi);
                var popup = new Mazemap.Popup({closeOnClick: true, offset: [0, -30]})
                .setHTML('<a href = "toilets/?id='+toiletid + '">' + poi.properties.title + '</a>');
            
            
                var mazeMarker = new Mazemap.MazeMarker({
                    color: '#ff00cc',
                    innerCircle: true,
                    innerCircleColor: '#FFF',
                    size: 34,
                    innerCircleScale: 0.5,
                    zLevel: poi.properties.zLevel,
                })
                .setPopup(popup)
                .setLngLat(lngLat)
                .addTo(map);    

                // If we have a polygon, use the default 'highlight' function to draw a marked outline around the POI.
                if(poi.geometry.type === "Polygon"){
                    map.highlighter.highlight(poi);
                }
            }     
        }); 
    }

    render() {
        let isLoading = this.state.loading;
        


        return (
            <>
            { isLoading ? <div>Loading map</div> : <MazeMapWrapper map={map} /> }
            </>
        )
    }
}
