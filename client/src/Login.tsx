import Button from '@restart/ui/esm/Button';
import * as React from 'react';
import { Card, Row } from 'react-bootstrap';
import { Alert } from './wigdets'
import { Link } from "react-router-dom";
import { Navigate } from "react-router-dom"

let url: string = "http://localhost:3001/api/v1"
type Test ={
    username : string;
    password : string;
    alert?: string | undefined;
}
type LoginProps ={
    from: string | "/";
    needAuth?: boolean;

}

export default class Login extends React.Component<LoginProps, Test> {
    state :Test = {
        username: "",
        password: "",
        alert: undefined
    }
    constructor(props: LoginProps){
        super(props)
    }
    render(){
        return(
            <div className="container" >
                {this.state.alert ? <Alert message={this.state.alert}/>:""}
                <h1> Login </h1>
                        <div className="form-group">
                            <label>
                                <p>Username:</p>
                                <input placeholder="Username" className="form-control" type="text" onChange={e => (this.setState({ username: e.target.value}))} />
                            </label>
                        </div>
                    <div className="form-group">
                        <label>
                            <p>Password</p>
                            <input  className="form-control" placeholder="password" type="password" onChange={e => (this.setState({ password: e.target.value}))}/>
                        </label>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary" type="submit" onClick={this.login.bind(this)}>Login</button>
                    </div><br/>
                    <div>
                        <p>Don't have an account sign up here: <Link to='/signup'>Sign up</Link></p>
                    </div>
            </div>
        )
    }
    componentDidMount(){
        let params:URLSearchParams = new URLSearchParams(document.location.search.substring(1));
        if(this.props.needAuth){
            this.setState({alert : "You must be logged in to perform that action"})
        }
       setTimeout(() => {
            this.setState({alert: undefined})
        }, 3000);
    }
    login(){
        setTimeout(() => {
            this.setState({alert: undefined})
        }, 2000);
        if(this.state.username == "" || this.state.password == ""){
            this.setState({alert : "You must spesify a username and password"})
            return
        }
        let data: Test = {
            username: this.state.username,
            password: this.state.password
        }
        fetch(url + "/login", {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            /* credentials: 'include', */
            /* mode: 'cors', // no-cors, *cors, same-origin */
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        })
            .then(response => {
                console.log(response)
                if(response.status != 200){
                    this.setState({alert : "Wrong username and password"})
                }
                else{
                    response.json()
                    .then((data) =>{
                        localStorage.setItem('jwt', data)
                        window.location.href="" + this.props.from
                    })
                        .catch(() => {
                            this.setState({alert : "Something went wrong"})
                        })
                }

            })
            .catch(() => {
                this.setState({alert : "Could not connect to the database"})
            })
    }
}
