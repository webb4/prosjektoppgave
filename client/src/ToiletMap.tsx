import * as React from 'react';
import { MazeMapWrapper, makeMazeMapInstance } from './mazemagic';
import { Mazemap } from "mazemap";

import { Toilet } from '../../types';

type MyProps = {
    
};
type MyState = { 
    toilets:Toilet[], 
    error: boolean, 
    loading: boolean 
};

let url: string = "http://localhost:3001/api/v1";

const campusId = 1;
const map = makeMazeMapInstance({campuses: campusId },"small");

export class ToiletMap extends React.Component<any, MyState> {
    mazeMarker: any = undefined;
    constructor(props: any) {
        super(props);
        this.state = {
            toilets: [],
            error: false,
            loading: true,
        }
    }


    componentDidMount() {
        console.log("componentDidMount"); //Check if component is mounted
        this.setState({ loading: false });

        map.on('click', (e: any) => { 
            this.click(e)
        });
          /* map.highlighter = new Mazemap.Highlighter( map, { */
          /*       showOutline: true, */
          /*       showFill: true, */
          /*       outlineColor: Mazemap.Util.Colors.MazeColors.MazeBlue, */
          /*       fillColor: Mazemap.Util.Colors.MazeColors.MazeBlue */
          /*   } ); */
    }
    click(e: any){
        console.log("click")
        console.log(e)
        console.log(e.lngLat)
        Mazemap.Data.getPoiAt(e.lngLat, map.zLevel).then((poi: any) =>{
            console.log(poi)
            console.log(poi.properties.poiId)
            this.clearPoiMarker(poi)
            this.placePoiMarker(poi)
            this.props.updatePoi(poi.properties.poiId)
        })
    }
    placePoiMarker(poi: any){

            // Get a center point for the POI, because the data can return a polygon instead of just a point sometimes
            var lngLat = Mazemap.Util.getPoiLngLat(poi);

            this.mazeMarker = new Mazemap.MazeMarker({
                color: '#ff00cc',
                innerCircle: true,
                innerCircleColor: '#FFF',
                size: 20,
                innerCircleScale: 0.5,
                zLevel: poi.properties.zLevel
            })
            .setLngLat(lngLat)
            .addTo(map);

            // If we have a polygon, use the default 'highlight' function to draw a marked outline around the POI.
            /* if(poi.geometry.type === "Polygon"){ */
            /*     map.highlighter.highlight(poi); */
            /* } */
            map.flyTo({center: lngLat, zoom: 19, speed: 0.5});
        }
        clearPoiMarker(poi: any){
            if(this.mazeMarker){
                this.mazeMarker.remove();
            }
            /* map.highlighter.clear(); */
        };
    render() {
        let isLoading = this.state.loading;
        return (
            <>
            { isLoading ? <div>Loading map</div> : <MazeMapWrapper map={map} /> }
            </>
        )
    }
}
