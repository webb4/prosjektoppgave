const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: "./src/index.tsx",
    stats: {
        children: true,
    },
    output: { 
        filename: "bundle.js" ,
        publicPath: "/",
        path: path.resolve(__dirname, "build"), 
    },
    resolve: { 
        extensions: ["*", ".tsx", ".ts", ".jsx", ".js"],
        alias: {
            "vendor": path.resolve(__dirname, "./vendor/"),
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "src", "index.html"),
            favicon: path.join(__dirname, "src", "favicon.ico"),
        }),
    ],
    devServer: { 
        static: path.join(__dirname, "src"),
        historyApiFallback: true
    },
    devtool: "inline-source-map",
    mode: "production",
    module: {
        rules: [
            {   test: /\.tsx?$/,
                use: ["ts-loader"],
                exclude: /node_modules|vendor/,
            },
            {
                test: /\.(js|jsx)$/,
                use: ["babel-loader"],
                exclude: /node_modules|vendor/,
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    },
                ],
            },
            {
                test: /\.(css)$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true,
                            importLoaders: 1,
                            modules: {
                                localIdentName: "[local]___[hash:base64:5]"
                            }
                        }
                    }
                ],
                exclude: /node_modules\/mazemap/,
            },
            {
                test: /\.(css)$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: false,
                            modules: false,
                        }
                    }
                ],
                include: /node_modules\/mazemap/,
            },
            {
                test: /\.react\.(svg)$/,
                use: [
                    { loader: "babel-loader" },
                    { loader: "react-svg-loader" },
                ],
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/,
                use: [
                    { loader: "file-loader" },
                ],
            },

        ],
    },
};
