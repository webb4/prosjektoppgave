CREATE TABLE
`fs_bdigsec_ToiletMap_test`.`Toilet` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, 
    `creator_user_id` INT(11) UNSIGNED NOT NULL,
    `name` VARCHAR(512) NOT NULL,
    `description` TEXT,
    `gender` INT DEFAULT 4,
    `facilities` TEXT,
    `poi_id` INT UNSIGNED NOT NULL,
    `campus_id` INT NOT NULL DEFAULT -1,
    `avg_rating` INT DEFAULT NULL,
    `calculated_avg` TINYINT(1) DEFAULT 0,
    `image_link` TEXT,
    PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE
`fs_bdigsec_ToiletMap_test`.`User` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(256) NOT NULL,
    `password` VARCHAR(512) DEFAULT NULL,
    `type` VARCHAR(32) NOT NULL DEFAULT 'user',
    PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE
`fs_bdigsec_ToiletMap_test`.`Review` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT(11) UNSIGNED NOT NULL,
    `toilet_id` INT UNSIGNED NOT NULL,
    `title` VARCHAR(64),
    `description` TEXT DEFAULT NULL,
    `toilet_rating` INT DEFAULT 10,
    `review_rating` INT UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE 
`fs_bdigsec_ToiletMap_test`.`ReviewVotes` ( 
    `user_id` INT UNSIGNED NOT NULL , 
    `review_id` INT UNSIGNED NOT NULL , 
    `vote` INT NOT NULL , 
    PRIMARY KEY (`user_id`, `review_id`)) ENGINE = InnoDB;
