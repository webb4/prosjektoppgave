# MyDass

## Installation manual

To use the application provided by To-Do-Lo there are some requirements:

- NodeJS
- A command line
- A copy of the project's folder downloaded

Table of contents:

[[_TOC_]]

### Installing nodeJS

Go to [NodeJS' website](https://nodejs.org/en/download/) then download and run the installer for your chosen system.

### Installing and running the program

When NodeJS is properly installed on your computer, download the program:  
![Image explaining the steps of downloading](https://media.discordapp.net/attachments/889408386393841685/912283708764069949/unknown.png)  
Do note that the project may look different to you, but the download button should be there.

Extract the .zip file to a folder and do the following based on your operating system:

Alternatively:

You can run `git clone https://gitlab.stud.idi.ntnu.no/webb4/prosjektoppgave.git` in your terminal.

### Install and run the app

After extracting the zipped folder (.tar, .zip etc.) open up your terminal application (terminal by default) and change the current directory to the application's files (This is done by typing in "cd {PATH_TO_PROJECT}").  
For this project you need two instances of a terminal, or you can run the process in the background:

In terminal 1:

```bat
cd server/
npm install
npm start
```

In terminal 2:

```bat
cd client/
npm install
npm start
```

Every time you want to run the application you need to "cd {project}/server" + "cd {project}/client" and "npm start"
