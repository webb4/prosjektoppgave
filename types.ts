export class Toilet{
    [key: string]: string | number | boolean | undefined;
    id: number | undefined;
    creator_user_id: number | undefined;
    name:string | undefined;
    description: string | undefined;
    gender: number | undefined;
    facilities: string | undefined;
    poi_id: string | undefined;
    campus_id: number | undefined;
    avg_rating: number | undefined;
    calculated_avg: number | string | boolean | undefined;
    image_link: string | undefined;
}
export class User{
    [key: string]: string | number | undefined ;
    id: number = -1;
    username: string = "";
    password: string | undefined = "";
    type: string = "";
}
export class Review{
    [key: string]: string | number | undefined ;
    id: number | undefined ;
    user_id: number | undefined;
    toilet_id: number | undefined;
    title: string | undefined;
    description: string | undefined;
    toilet_rating: number | undefined;
    review_rating: number | undefined;
    username: string | undefined;
}
export class ReviewVotes{
    [key: string]: number | undefined;
    user_id: number | undefined;
    review_id: number | undefined;
    vote: number | undefined;
}
export class Rating{
    [key: string]: string | number;
    "AVG(\"toilet_rating\")": string | number;
}

export class Vote{
    [key: string]: string | number;
    "SUM(vote)": string | number;
}